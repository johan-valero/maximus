from django.contrib import admin
from .models import Bodybuilder

# Register your models here.
admin.site.register(Bodybuilder)