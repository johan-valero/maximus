from django.urls import path
from . import views

app_name = 'muscle'
urlpatterns = [
    path('', views.index, name='index'),
]
