from django.db import models

# Create your models here.
class Bodybuilder(models.Model):
    name_bodybuilder = models.CharField(max_length=50)
    firstname_bodybuilder = models.CharField(max_length=50)
    birthdate_bodybuilder = models.DateField("Date de naissance du bodybuilder")
    picture_bodybuilder = models.ImageField(upload_to="images", blank=True)

    def __str__(self):
        affiche = f"{self.firstname_bodybuilder} {self.name_bodybuilder}"
        return affiche