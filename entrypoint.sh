#!/bin/sh

if [ "$DATABASE" = "mariadb" ]
then
    echo "Waiting for mariadb..."

    while ! nc -z $MARIADB_HOST $MARIADB_PORT; do
        sleep 0.1
    done

    echo "Mariadb started"

    echo "GRANT ALL on *.* to '${MARIADB_USER}';"| mysql -u root --password="${MARIADB_ROOT_PASSWORD}" -h "${MARIADB_HOST}"

fi


python3 manage.py flush --no-input
python3 manage.py migrate
#python3 manage.py shell < script.py
#python3 manage.py loaddata db.json
python3 manage.py collectstatic --noinput

exec "$@"
